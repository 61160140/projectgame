// select all elements
const start = document.getElementById("start");
const quiz = document.getElementById("quiz");
const qImg = document.getElementById("qImg");
const choiceA = document.getElementById("A");
const choiceB = document.getElementById("B");
const choiceC = document.getElementById("C");
const counter = document.getElementById("counter");
const timeGauge = document.getElementById("timeGauge");
const progress = document.getElementById("progress");
const scoreDiv = document.getElementById("scoreContainer");

// create our questions
let questions = [
    {
        
        imgSrc : "img/quize/game16.jpg",
        choiceA : "รดตู้",
        choiceB : "ลดตู้",
        choiceC : "รถตู้",
        correct : "C"
    },{
        
        imgSrc : "img/quize/game17.jpg",
        choiceA : "รดม้า",
        choiceB : "รถม้า",
        choiceC : "ลดม้า",
        correct : "B"
    },{
        
        imgSrc : "img/quize/game18.jpg",
        choiceA : "รหัส",
        choiceB : "รหัศ",
        choiceC : "ลหัส",
        correct : "A"
    },{
        
        imgSrc : "img/quize/game19.jpg",
        choiceA : "ขมิ้น",
        choiceB : "คมิ้น",
        choiceC : "ขมิน",
        correct : "A"
    },{
        
        imgSrc : "img/quize/game20.jpg",
        choiceA : "มน",
        choiceB : "มนต์",
        choiceC : "มณต์",
        correct : "B"
    },{
        
        imgSrc : "img/quize/game21.jpg",
        choiceA : "มนดำ",
        choiceB : "มดดำ",
        choiceC : "มดตำ",
        correct : "B"
    },{
        
        imgSrc : "img/quize/game22.jpg",
        choiceA : "มดแดง",
        choiceB : "มนแดง",
        choiceC : "มดแฎง",
        correct : "A"
    },{
        
        imgSrc : "img/quize/game23.jpg",
        choiceA : "จัมลอง",
        choiceB : "จำรอง",
        choiceC : "จำลอง",
        correct : "C"
    },{
        
        imgSrc : "img/quize/game24.jpg",
        choiceA : "ฟักธอง",
        choiceB : "ฟักทอง",
        choiceC : "ฟักฑอง",
        correct : "B"
    },{
        
        imgSrc : "img/quize/game25.jpg",
        choiceA : "ของเล่น",
        choiceB : "ของเร่น",
        choiceC : "ของเล่ณ",
        correct : "A"
    },{
        
        imgSrc : "img/quize/game26.jpg",
        choiceA : "ฟิสิกส์",
        choiceB : "ฟิสิขส์",
        choiceC : "ฟิศิกส์",
        correct : "A"
    },{
        
        imgSrc : "img/quize/game27.jpg",
        choiceA : "สนิ่ม",
        choiceB : "สนิม",
        choiceC : "สนิ้ม",
        correct : "B"
    },{
        
        imgSrc : "img/quize/game28.jpg",
        choiceA : "ไบใม้",
        choiceB : "ไบไม้",
        choiceC : "ใบไม้",
        correct : "C"
    },{
        
        imgSrc : "img/quize/game29.jpg",
        choiceA : "ถุงเฑ้า",
        choiceB : "ถุงเท้า",
        choiceC : "ถุงท้าว",
        correct : "B"
    },{
        
        imgSrc : "img/quize/game30.jpg",
        choiceA : "ผม",
        choiceB : "พม",
        choiceC : "พหรม",
        correct : "A"
    }
];

// create some variables

const lastQuestion = questions.length - 1;
let runningQuestion = 0;
let count = 0;
const questionTime = 10; // 10s
const gaugeWidth = 150; // 150px
const gaugeUnit = gaugeWidth / questionTime;
let TIMER;
let score = 0;

// render a question
function renderQuestion(){
    let q = questions[runningQuestion];
    
    //question.innerHTML = "<p>"+ q.question +"</p>";
    qImg.innerHTML = "<img src="+ q.imgSrc +">";
    choiceA.innerHTML = q.choiceA;
    choiceB.innerHTML = q.choiceB;
    choiceC.innerHTML = q.choiceC;
}

start.addEventListener("click",startQuiz);

// start quiz
function startQuiz(){
    start.style.display = "none";
    renderQuestion();
    quiz.style.display = "block";
    renderProgress();
    renderCounter();
    TIMER = setInterval(renderCounter,1000); // 1000ms = 1s
}

// render progress
function renderProgress(){
    for(let qIndex = 0; qIndex <= lastQuestion; qIndex++){
        progress.innerHTML += "<div class='prog' id="+ qIndex +"></div>";
    }
}

// counter render

function renderCounter(){
    if(count <= questionTime){
        counter.innerHTML = count;
        timeGauge.style.width = count * gaugeUnit + "px";
        count++
    }else{
        count = 0;
        // change progress color to red
        answerIsWrong();
        if(runningQuestion < lastQuestion){
            runningQuestion++;
            renderQuestion();
        }else{
            // end the quiz and show the score
            clearInterval(TIMER);
            scoreRender();
        }
    }
}

// checkAnwer

function checkAnswer(answer){
    if( answer == questions[runningQuestion].correct){
        // answer is correct
        score++;
        // change progress color to green
        answerIsCorrect();
    }else{
        // answer is wrong
        // change progress color to red
        answerIsWrong();
    }
    count = 0;
    if(runningQuestion < lastQuestion){
        runningQuestion++;
        renderQuestion();
    }else{
        // end the quiz and show the score
        clearInterval(TIMER);
        scoreRender();
    }
}

// answer is correct
function answerIsCorrect(){
    document.getElementById(runningQuestion).style.backgroundColor = "#0f0";
}

// answer is Wrong
function answerIsWrong(){
    document.getElementById(runningQuestion).style.backgroundColor = "#f00";
}

// score render
function scoreRender(){
    scoreDiv.style.display = "block";
    
    // calculate the amount of question percent answered by the user
    const scorePerCent = Math.round(100 * score/questions.length);
    
    // choose the image based on the scorePerCent
    let img = (scorePerCent >= 80) ? "img/5.png" :
              (scorePerCent >= 60) ? "img/4.png" :
              (scorePerCent >= 40) ? "img/3.png" :
              (scorePerCent >= 20) ? "img/2.png" :
              "img/1.png";
    
    scoreDiv.innerHTML = "<img src="+ img +">";
    scoreDiv.innerHTML += "<p>"+ scorePerCent +" คะแนน</p>";
}





















