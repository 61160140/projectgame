let scoreCor = 0
let scoreNeg = 0

function getScore () {
  scoreCor = localStorage.getItem('correctNum')
  scoreNeg = localStorage.getItem('negativeNum')
}

function countCorrect () {
  getScore()
  scoreCor++
  localStorage.setItem('correctNum', scoreCor)
}

function countNegative () {
  getScore()
  scoreNeg++
  localStorage.setItem('negativeNum', scoreNeg)
}

function init () {
  getScore()
  if (scoreCor == 10) {
    document.getElementById('star1').src = './../image/starGame/full_star.png'
    document.getElementById('star2').src = './../image/starGame/full_star.png'
    document.getElementById('star3').src = './../image/starGame/full_star.png'
  } else if (scoreCor >= 5) {
    document.getElementById('star1').src = './../image/starGame/full_star.png'
    document.getElementById('star2').src = './../image/starGame/full_star.png'
  } else if (scoreCor > 0) {
    document.getElementById('star1').src = './../image/starGame/full_star.png'
  }
  document.getElementById('scoreText').innerHTML =
    'ถูก : ' +
    localStorage.getItem('correctNum') +
    ' ข้อ | ผิด : ' +
    localStorage.getItem('negativeNum') +
    ' ข้อ'
  //   console.log(
  //     'ถูก : ' +
  //       localStorage.getItem('correctNum') +
  //       ' ผิด : ' +
  //       localStorage.getItem('negativeNum')
  //   )
}

function resetScore () {
  scoreCor = localStorage.setItem('correctNum', 0)
  scoreNeg = localStorage.setItem('negativeNum', 0)
}
